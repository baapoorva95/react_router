import React from 'react';
import './App.css';
import {Routes, Route } from "react-router-dom";
import{
  Home,
  About,
  Event,
  Contact
}from "./pages"

function App() {
  return (
    <div className="App">
     <Routes>
       <Route path="/" element={<Home/>}/>
       <Route path="/about" element={<About/>}/>
       <Route path="/event" element={<Event/>}/>
       <Route path="/contact" element={<Contact/>}/>
     </Routes>
    </div>
  );
}

export default App;
